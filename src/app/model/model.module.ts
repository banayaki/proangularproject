import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ProductRepository} from './product-repository';
import {StaticDataSource} from './static-data-source.service';
import {Cart} from './cart';
import {Order} from './order';
import {OrderRepository} from './order-repository';
import {HttpClientModule} from '@angular/common/http';
import {RestDataSource} from './rest-data-source';
import {AuthService} from './auth.service';

@NgModule({
  declarations: [],
  providers: [
    ProductRepository,
    // StaticDataSource,
    Cart,
    Order,
    OrderRepository,
    {provide: StaticDataSource, useClass: RestDataSource},
    AuthService,
    RestDataSource
  ],
  imports: [
    CommonModule,
    HttpClientModule
  ]
})
export class ModelModule { }
