import {Injectable} from '@angular/core';
import {RestDataSource} from './rest-data-source';
import {Observable} from 'rxjs';

@Injectable()
export class AuthService {

  constructor(private dataSource: RestDataSource) {
  }

  get authenticated(): boolean {
    return this.dataSource.authToken != null;
  }

  authenticate(username: string, pass: string): Observable<boolean> {
    return this.dataSource.authenticate(username, pass);
  }

  clear() {
    this.dataSource.authToken = null;
  }
}
