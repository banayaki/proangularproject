import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {StoreModule} from './store/store.module';
import {RouterModule} from '@angular/router';
import {StoreComponent} from './store/store.component';
import {CartDetailComponent} from './store/cart-detail/cart-detail.component';
import {CheckoutComponent} from './store/checkout/checkout.component';
import {StoreFirstGuard} from './guards/store-first.guard';

const routes = [
  {path: 'admin', loadChildren: './admin/admin.module#AdminModule', canActivate: [StoreFirstGuard]},
  {path: 'store', component: StoreComponent, canActivate: [StoreFirstGuard]},
  {path: 'cart', component: CartDetailComponent, canActivate: [StoreFirstGuard]},
  {path: 'checkout', component: CheckoutComponent, canActivate: [StoreFirstGuard]},
  {path: '**', redirectTo: '/store'},

];

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    StoreModule,
    RouterModule.forRoot(routes)
  ],
  providers: [
    StoreFirstGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
