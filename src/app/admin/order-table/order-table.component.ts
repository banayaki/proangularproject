import {Component, OnInit} from '@angular/core';
import {OrderRepository} from '../../model/order-repository';
import {Order} from '../../model/order';

@Component({
  selector: 'app-order-table',
  templateUrl: './order-table.component.html',
  styleUrls: ['./order-table.component.css']
})
export class OrderTableComponent implements OnInit {
  includeShipped = false;

  constructor(private orderRepository: OrderRepository) {
  }

  getOrders(): Order[] {
    return this.orderRepository.getOrders()
      .filter(o => this.includeShipped || !o.shipped);
  }

  markShipped(order: Order) {
    order.shipped = true;
    this.orderRepository.updateOrder(order);
  }

  delete(id: number) {
    this.orderRepository.deleteOrder(id);
  }

  ngOnInit() {
  }

}
