import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {NgForm} from '@angular/forms';
import {AuthService} from '../../model/auth.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {
  public username: string;
  public password: string;
  public errormessage: string;

  constructor(private router: Router, private auth: AuthService) {
  }

  authenticate(form: NgForm) {
    if (form.valid) {
      console.log(`${this.username} ${this.password}`);
      this.auth.authenticate(this.username, this.password)
        .subscribe(response => {
          if (response) {
            this.router.navigateByUrl('/admin/main');
          } else {
            this.errormessage = 'Authentication Failed';
          }
        });
    } else {
      this.errormessage = 'From data invalid';
    }
  }

  ngOnInit() {
  }

}
