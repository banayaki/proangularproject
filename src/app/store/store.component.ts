import { Component, OnInit } from '@angular/core';
import {Product} from '../model/product';
import {ProductRepository} from '../model/product-repository';
import {Cart} from '../model/cart';
import {Router} from '@angular/router';

@Component({
  selector: 'app-store',
  templateUrl: './store.component.html',
  styleUrls: ['./store.component.css']
})
export class StoreComponent implements OnInit {
  public selectedCategory = null;
  public productPerPage = 4;
  public selectedPage = 1;

  constructor(private repository: ProductRepository,
              private cart: Cart,
              private router: Router) { }

  get products(): Product[] {
    let pageIndex = (this.selectedPage - 1) * this.productPerPage;
      return this.repository.getProducts(this.selectedCategory)
      .slice(pageIndex, this.productPerPage + pageIndex);
  }

  get categories(): string[] {
    return this.repository.getCategories();
  }

  changeCategory(newCategory?: string): void {
    this.selectedCategory = newCategory;
  }

  changePage(newPage: number): void {
    this.selectedPage = newPage;
  }

  changePageSize(newSize: number): void {
    this.productPerPage = Number(newSize);
    this.changePage(1);
  }

  addProductToCart(product: Product) {
    this.cart.addLine(product);
    this.router.navigateByUrl('/cart');
  }

  get pageCount(): number {
    return Math.ceil(this.repository.getProducts(this.selectedCategory).length / this.productPerPage);
  }

  ngOnInit() {
  }

}
