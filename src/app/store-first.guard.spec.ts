import {inject, TestBed} from '@angular/core/testing';

import {StoreFirstGuard} from './guards/store-first.guard';

describe('StoreFirstGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StoreFirstGuard]
    });
  });

  it('should ...', inject([StoreFirstGuard], (guard: StoreFirstGuard) => {
    expect(guard).toBeTruthy();
  }));
});
